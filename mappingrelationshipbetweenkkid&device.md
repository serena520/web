###### 金刚梯>金刚帮助>金刚1.0金刚号梯帮助>术语 金刚号类>
### 问：金刚号可用于哪些设备？

答：

- [ 普通金刚号 ](/singlepurposekkid.md)仅可用于Windows+SSL客户端环境，不可用于其他设备
- [ 万能金刚号 ](/multipurposekkid.md)可用于[主流智能设备](/list_kkproducts1.0.md)
- [ 万能金刚号 ](/multipurposekkid.md)可按[ 一拖九 ](/onefornine.md)方式使用

#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [术语 金刚号类](/list_kkid.md)
