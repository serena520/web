###### 玩转金刚>金刚字典>

### 金刚号

- 所谓<strong> 金刚号 </strong>是：
  - [ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)使用[ 金刚服务 ](/LadderFree/kkDictionary/KKServices.md)的凭证
  - [ 金刚公司 ](/list_kk.md)向[ 金刚用户 ](/kkuser.md)提供[ 金刚服务 ](/kkservices.md)的管道
- <strong> 作用和价值</strong>
  - 类似于手机电话号码 
- <strong> 金刚号 </strong>的形态
  - 以字母c开头，后接5～10位阿拉伯数字
  - 比如： c57314692 即是一个金刚号
- <strong> 金刚号 </strong> 分为:
  - [ 普通金刚号 ](/singlepurposekkid.md)
  - [ 万能金刚号 ](/multipurposekkid.md)
- 在安卓手机里，填入<strong> 金刚号 </strong>的位置叫<strong> 用户名 </strong> <br>
![image](/B073B1E6-B647-48FA-8931-35923C5EA54F.jpeg)
- 在苹果手机里，填入<strong> 金刚号 </strong>的位置叫<strong>  帐户 </strong> <br>
![image](/24491F5B-F762-4C61-AB73-50B2F409CF92.jpeg)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

