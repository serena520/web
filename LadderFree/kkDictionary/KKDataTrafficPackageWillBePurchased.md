###### 玩转金刚梯>金刚字典>
### 拟购流量包
- 所谓<strong> 拟购流量包 </strong >是指[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)打算立即购买的[ 金刚流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

