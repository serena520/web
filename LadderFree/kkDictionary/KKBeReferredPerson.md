###### 玩转金刚梯>金刚字典>
### 被推荐人
- 所谓<strong> 被推荐人 </strong >是指经由:
  - 扫描[ 金刚广告商 ](/LadderFree/kkDictionary/KKAdvertiser.md)或[ 金刚推荐人 ](/LadderFree/kkDictionary/KKReferrer.md)散发的[ 邀请二维码 ](/LadderFree/kkDictionary/KKInvitationQRCode.md) 或
  - 点击[ 金刚广告商 ](/LadderFree/kkDictionary/KKAdvertiser.md)或[ 金刚推荐人 ](/LadderFree/kkDictionary/KKReferrer.md)散发的[ 邀请链接 ](/LadderFree/kkDictionary/KKInvitationLink.md)

- 到达[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)并[ 注册 ](/l2_reg.md)成功的[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

