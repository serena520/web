###### 玩转金刚梯>金刚字典>

### 初始密码
- <strong> 初始密码 </strong>是您的[ 金刚账户 ](/LadderFree/kkDictionary/KKAccount.md)的第一个密码
- <strong> 初始密码 </strong>由7位阿拉伯数字构成
- <strong> 初始密码 </strong>由[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)自动生成
- [ 注册 ](/l2_reg.md)成功后，[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)立即将[ 用户名 ](/LadderFree/kkDictionary/KKSiteZhsUserName.md)和<strong> 初始密码 </strong>用《金刚网新账户邮件》送达您的[ 注册邮箱 ](/LadderFree/kkDictionary/RegistrationEmailaddressAtKKSiteZh.md)
- 建议您据《派号通知单》邮件，尽快配好、[ 连通 ](/LadderFree/kkDictionary/KKIDsUsage.md)[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)，进而立即[ 重置密码 ](https://www.atozitpro.net/zh/password-reset/)，将<strong> 初始密码 </strong >变更为易记的密码


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

