###### 玩转金刚梯>金刚字典>

### 密码
- 此处所谓<strong> 密码 </strong>是指您登录[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)时所需的<strong> 密码 </strong>，而非当您在手机（或其他设备）上填入[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)等时所填的[ 金刚密码 ](/LadderFree/kkDictionary/KKIDsPassWord.md)
- 关于该<strong > 密码 </strong >的<strong> 初始密码 </strong>的定义，请[ 点击此处 ](/LadderFree/kkDictionary/KKSiteZhsInitialPasswd.md)获取
 

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
